<?php
/**
 * Plugin Name: Tebura Content Publisher
 * Plugin URI: https://samepagenet.com/
 * Description: Add Shortcode for publishing tebura content on WordPress Website.
 * Version: 1.0.0
 * Author: SamePage Inc.
 * Author URI: http://samepagenet.com
 * Requires at least: 4.2
 * Tested up to: 4.8
 * License: GPLv3 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * Text Domain: sp-tebura-content
 * Domain Path: /languages/
 *
 * @package Tebura Content Publisher
 * @author Kudratullah <mhamudul.hk@gmail.com>
 *
 */
/**
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	Copyright 2017 SamePage Inc.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class SP_Tebura_Content_Publisher {
	public $textdomain = "sp-irodorifactory";
	/**
	 * Holds self instance
	 * @var \SP_Tebura_Content_Publisher
	 */
	static $instance;
	/**
	 * is shortcode rendared
	 * @var boolean
	 */
	static $isRendered;
	/**
	 * Publisher JS Version
	 * @var string
	 */
	static $scriptVar = "1.2.0";
	/**
	 * Option object for Publisher.js
	 * @var string
	 */
	static $options;
	/**
	 * Publisher JS Source URL
	 * @var string
	 */
	static $jsPluginSrc = "https://tebura.ninja/publisher/js";
	/**
	 * 
	 * Initialize SP_Tebura_Content_Publisher
	 * @return \SP_Tebura_Content_Publisher
	 *
	 */
	public static function init() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	/**
	 * Constructor
	 * @return void
	 */
	public function __construct(){
		// load plugin i18n
		load_plugin_textdomain( $this->textdomain, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		add_action( 'wp_enqueue_scripts',  array( $this, 'registerScriptsAndStyles' ), 10 );
		add_shortcode( 'tebura_content', array( $this, 'shortcode' ), 10 );
	}
	public function shortcode( $atts ) {
		$atts = shortcode_atts(
				array(
						'selector'   => '.tebura_content',
						'id'         => '',
						'latitude'   => '',
						'longitude'  => '',
						'view'       => '',
						'language'   => 'en',
				),
				$atts, 'tebura_content' );
		$atts = array_filter( $atts );
		// if( ! isset( $atts['selector'] ) || ! isset( $atts['latitude'] ) || ! isset( $atts['longitude'] ) ) return "";
		$output  = '<div class="'. str_replace('.', '', $atts['selector']).'"></div>';
		self::$options = "var tebura = " . json_encode( $atts ) . ";";
		wp_add_inline_script( 'teburaPublisherJS', self::$options, "before" );
		wp_enqueue_script( 'teburaPublisherJS' );
		return $output;
	}
	public function get_opts() {
		return self::$options;
	}
	public function registerScriptsAndStyles() {
		wp_register_script( 'teburaPublisherJS', self::$jsPluginSrc, array(), self::$scriptVar, true);
		
	}
}
$spt_content_publisher = SP_Tebura_Content_Publisher::init();
// End Of File sp-tebura-content.php