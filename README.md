# Tebura Content Publisher
Tags: Content, Publisher, Affiliate, Sightseeing, Place of Interest, Luggage Storage
Requires at least: 4.2
Tested up to: 4.8
Requires PHP: 5.6
Stable tag: trunk
Version: 1.0.2
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl.txt

Show nearby Coin locker, Luggage Storage Place, Place of Interest in your website.

## Description
This plugin can show nearby coin locker service, luggage storage service, place of interest etc. by geo location (Latitude and Longitude) in WordPress site via shortcode.
Currently this plugin supports English and Japanese Content with 2 different theme.
You can suggest us for more theme/feature via this plugin\'s support, this repository or our website https://tebura.ninja or http://samepagenet.com

we also have support for other platform by JavaScript API.
For Documentation about this plugin and JavaScript API please visit https://tebura.ninja/content-publisher/
Other links:
http://samepagenet.com
https://bitbucket.org/samepage/tebura-content-publisher

## Installation
**This section describes how to install the plugin and get it working:**
1. Download & unzip to WordPress plugin directory.
2. Login to WordPress admin panel and active the plugin.
3. Activate the plugin.
4. Open any page or post in WP-Admin Editor and use `[tebura_content]` shortcode.
5. Click \'Update\' or \'Publish\' to save the changes. Now you can open the page/post to see the contents.

## Update
**Use WordPress Automatic Update Option.**

## Frequently Asked Questions
See https://tebura.ninja/faq/

## Screenshots
![Minimal Default Theme](screenshot-1.png)
![Alternative Theme with Nearby Place of Interest (Geo Location Supported )](screenshot-2.png)

## Changelog
* 1.0.0
    * Initial Release.
* 1.0.1
    * Example files added.
* 1.0.2
    * Fixed attribute and characters in example file.